import { Box, VStack, Heading, Button, useColorModeValue, Center, Grid, GridItem, Text, Image, IconButton, HStack, Avatar, Badge, Stack, Divider, Flex, Spacer, Input, Select, Menu, MenuButton, MenuList, MenuItem, MenuDivider, Tooltip, Tabs, TabList, TabPanels, Tab, TabPanel, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Drawer, DrawerBody, DrawerFooter, DrawerHeader, DrawerOverlay, DrawerContent, DrawerCloseButton, useDisclosure } from '@chakra-ui/react';
import { FaHeart, FaStar, FaSearch, FaFilter, FaMapMarkerAlt, FaCalendarAlt, FaUserFriends } from 'react-icons/fa';
import { BrandLogo } from '@components/BrandLogo';
import { useState } from 'react';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const listings = [
    {
      id: 1,
      title: 'Cozy Cottage',
      location: 'San Francisco, CA',
      price: '$120/night',
      rating: 4.8,
      reviews: 34,
      imageUrl: 'https://picsum.photos/400/300',
      host: {
        name: 'John Doe',
        avatarUrl: 'https://i.pravatar.cc/100'
      }
    },
    {
      id: 2,
      title: 'Modern Apartment',
      location: 'New York, NY',
      price: '$200/night',
      rating: 4.9,
      reviews: 45,
      imageUrl: 'https://picsum.photos/400/300',
      host: {
        name: 'Jane Smith',
        avatarUrl: 'https://i.pravatar.cc/100'
      }
    }
  ];

  return (
    <Center bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8" w="full">
        <BrandLogo />
        <Heading as="h1" size="xl" textAlign="center">
          Welcome to AirBnB Clone!
        </Heading>
        <Flex w="full" alignItems="center" justifyContent="space-between" mb={8}>
          <Input placeholder="Search listings" size="lg" w={{ base: 'full', md: '60%' }} />
          <Spacer />
          <Menu>
            <Tooltip label="Filter options" aria-label="Filter options">
              <MenuButton as={IconButton} icon={<FaFilter />} size="lg" colorScheme="pink" ml={4} />
            </Tooltip>
            <MenuList>
              <MenuItem>Price: Low to High</MenuItem>
              <MenuItem>Price: High to Low</MenuItem>
              <MenuDivider />
              <MenuItem>Rating: High to Low</MenuItem>
              <MenuItem>Rating: Low to High</MenuItem>
            </MenuList>
          </Menu>
        </Flex>
        <Tabs variant="soft-rounded" colorScheme="pink" w="full">
          <TabList>
            <Tab>Unterkünfte</Tab>
            <Tab>Entdeckungen</Tab>
            <Tab>Online-Entdeckungen</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <Grid templateColumns={{ base: '1fr', md: 'repeat(2, 1fr)', lg: 'repeat(3, 1fr)' }} gap={6} w="full">
                {listings.map((listing) => (
                  <GridItem key={listing.id} bg="white" shadow="md" borderRadius="md" overflow="hidden">
                    <Image src={listing.imageUrl} alt={listing.title} w="full" h="200px" objectFit="cover" />
                    <Box p={4}>
                      <HStack justifyContent="space-between" mb={2}>
                        <Heading as="h3" size="md">
                          {listing.title}
                        </Heading>
                        <IconButton
                          aria-label="Add to favorites"
                          icon={<FaHeart />}
                          variant="ghost"
                          colorScheme="pink"
                        />
                      </HStack>
                      <Text fontSize="sm" color="gray.500">
                        {listing.location}
                      </Text>
                      <Text fontSize="lg" fontWeight="bold" mt={2}>
                        {listing.price}
                      </Text>
                      <HStack mt={2} alignItems="center">
                        <FaStar color="gold" />
                        <Text fontSize="sm" ml={1}>
                          {listing.rating} ({listing.reviews} reviews)
                        </Text>
                      </HStack>
                      <Divider my={4} />
                      <HStack>
                        <Avatar src={listing.host.avatarUrl} name={listing.host.name} />
                        <Stack spacing={0}>
                          <Text fontWeight="bold">{listing.host.name}</Text>
                          <Badge colorScheme="green">Superhost</Badge>
                        </Stack>
                      </HStack>
                    </Box>
                  </GridItem>
                ))}
              </Grid>
            </TabPanel>
            <TabPanel>
              <Text>Entdeckungen content goes here...</Text>
            </TabPanel>
            <TabPanel>
              <Text>Online-Entdeckungen content goes here...</Text>
            </TabPanel>
          </TabPanels>
        </Tabs>
        <Button colorScheme="pink" onClick={onOpen}>Als Gastgeber:in loslegen</Button>
        <Drawer isOpen={isOpen} placement="right" onClose={onClose}>
          <DrawerOverlay />
          <DrawerContent>
            <DrawerCloseButton />
            <DrawerHeader>Gastgeber:in werden</DrawerHeader>
            <DrawerBody>
              <Input placeholder="Wohin" mb={4} />
              <Input placeholder="Reiseziele suchen" mb={4} />
              <Input placeholder="Check-in Datum hinzufügen" mb={4} />
              <Input placeholder="Check-out Datum hinzufügen" mb={4} />
              <Input placeholder="Wer" mb={4} />
              <Input placeholder="Gäste hinzufügen" mb={4} />
            </DrawerBody>
            <DrawerFooter>
              <Button variant="outline" mr={3} onClick={onClose}>Cancel</Button>
              <Button colorScheme="pink">Submit</Button>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>
      </VStack>
    </Center>
  );
};

export default Home;